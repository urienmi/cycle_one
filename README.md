# Projet Cycle One

## Membres du projet

### Tuteurs

- Arnaud MARTIN
- Jean-Christophe DUBOIS
- Laura GELEY

### Développeurs

- Mickaël URIEN
- Quentin LUCCHINI

## Définition du projet

### Contexte

Cycle One est une jeune startup française créée en 2018. Elle commercialise des produits
lumineux pour cyclistes. Tous les produits sont connectés et configurables sur une application
smartphone (choix des lumières, guidade GPS par vibration, clignotants automatiques...). Les
produits permettent non seulement d’être mieux visibles sur la route mais aussi d’indiquer
les directions au cycliste et aux automobilistes. Leur sac à dos a été élu meilleure innovation
sportive en 2020 et la marque compte beaucoup sur le bouche à oreille pour augmenter ses
ventes, notamment via les réseaux sociaux (Facebook et Instagram). 10€ seront o#erts sur
toutes les commandes grâce au code INNOVATION2020.

### Le catalogue

Cycle One compte à ce jour 3 produits :
- un sac à dos ;
- un casque ;
- un t-shirt

Le sac à dos et le casque sont disponibles en noir, bleu, rouge, jaune ou orange (unisexe, taille
unique). Les t-shirts sont disponibles en noir, gris ou blanc (version homme et femme, taille
S, M, L ou XL). 

Cycle One souhaite offrir aux utilisateurs la possibilité d’acheter les produits
individuellement ou en kit (les 3 produits proposés avec 50€ de réduction).

Cycle One veut évidemment communiquer sur son concept et le fonctionnement de ses
produits (configuration de l’application, fonctionnalités...). Il est important pour leur équipe
d’éduquer très concrètement les utilisateurs et de donner vie au mode d’emploi. 

La marque souhaite également mettre en valeur ses 3 créations à travers des fiches produits type landing-
pages, ultra-détaillées et à forte inspiration publicitaire. L’idée est de donner une image jeune, moderne et high-tech.

Il est à noter que les utilisateurs auront également la possibilité de venir essayer les produits
dans l’une des 150 boutiques partenaires de la marque.