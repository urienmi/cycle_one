let manual_section = document.getElementById('manual');
let carac_section = document.getElementById('technical');
let fb_section = document.getElementById('feedback');

let manual_item = document.getElementById('manual-item');
let carac_item = document.getElementById('technical-item');
let fb_item = document.getElementById('feedback-item');

manual_section.style.display = "block";
carac_section.style.display = "none";
fb_section.style.display = "none";
manual_item.classList.add('active');
function showManualSection(){
    manual_section.style.display = "block";
    carac_section.style.display = "none";
    fb_section.style.display = "none";

    manual_item.classList.add('active');
    carac_item.classList.remove('active');
    fb_item.classList.remove('active');

}

function showCaracSection(){
    manual_section.style.display = "none";
    carac_section.style.display = "block";
    fb_section.style.display = "none";

    manual_item.classList.remove('active');
    carac_item.classList.add('active');
    fb_item.classList.remove('active');
}

function showFbSection(){
    manual_section.style.display = "none";
    carac_section.style.display = "none";
    fb_section.style.display = "block";

    manual_item.classList.remove('active');
    carac_item.classList.remove('active');
    fb_item.classList.add('active');
}




let caracTitles = document.getElementsByClassName('tech-title');

for (let element of caracTitles){
    element.addEventListener('click', function(){
        element.childNodes[3].childNodes[1].classList.toggle('rotate');
    })
}

$(function () {
	var inputs = $(".input");
	var paras = $(".description-flex-container").find("div.timeline-content");
	inputs.click(function () {
		var t = $(this),
			ind = t.index(),
			matchedPara = paras.eq(ind);

		t.add(matchedPara).addClass("active");
		inputs.not(t).add(paras.not(matchedPara)).removeClass("active");
	});
});

/* Quantity */

let btnPlus = document.getElementById('quantity-increment');
let btnLess = document.getElementById('quantity-decrement');

let inputQuantity = document.getElementById('quantity-input');

btnLess.addEventListener('click', (event)=>{
    if (inputQuantity.value > 1){
        inputQuantity.value -= 1;
    }
})
btnPlus.addEventListener('click', (event)=>{
    inputQuantity.value = parseInt(inputQuantity.value) + 1;
})

