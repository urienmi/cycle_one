function countdownTimer() {
    setInterval(function(){
        const diff = +new Date("2020-12-31") - +new Date();
        let remaining = '00d 00h 00m 00s';
        if (diff > 0) {
            const parts = {
                j: Math.floor( diff / (1000 * 24 * 60 * 60) ),
                h: Math.floor(( diff / (1000 * 60 * 60)) % 24),
                m: Math.floor(( diff / 1000 / 60) % 60),
                s: Math.floor(( diff / 1000 ) % 60),
            };
            remaining = Object.keys(parts).map(part => {
                if(!parts[part]) return;
                return `${parts[part]}${part}`;
            }).join(" ");
        }
        document.getElementById('countdown').innerHTML = remaining;
        }, 1000);
}
