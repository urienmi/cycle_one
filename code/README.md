# Cycle One

## /!\ IMPORTANT /!\
### Try to do this before each session
#### git merge master

### Watch with Sass
#### sass --watch assets/scss/:assets/css/

## Notes for Git

### Switch branches
#### git checkout <branch_name>

### You made changes on your branch
#### git add -> git commit -m "" -> git push origin <your_branch_name>

### Your changes are safe and you want to merge on master
#### git checkout master
#### git merge <branch_name>
